#include "table.h"
using namespace std;

//These are the functions that you will be writing for Lab #10


//STEP 1.1
int table::count()	//SAMPLE wrapper function
{
	return count(root);
}

int table::count(node * root){
    if ( !root )
        return 0;
    else{
        return 1 + count( root -> right ) + count( root -> left);
    }
}


//STEP 1.2
int table::count_leaf(){
    return count_leaf(root);
}

int table::count_leaf(node * root){
    if ( !root ){
        return 0;
    }
    else{ 
        if ( root -> left == NULL && root -> right == NULL )
            return 1;
        else{
            return count_leaf( root -> left) + count_leaf(root -> right);
        }
    }
}


//STEP 1.3
int table::remove_largest(){
    return remove_largest(root);
}
int table::remove_largest(node * & root){
    node* cur = root ;
    node* prev = root;
    while ( cur -> right != NULL){
        prev = cur;
        cur = cur -> right ;
    }
    if ( cur -> left == NULL ){
        prev -> right = NULL;
        delete cur ;
        return 1;
    }
    else{
        prev -> right = cur -> left;
        delete cur ;
        return 1;
    }
    return 0;
}


//STEP 1.4
int table::sum(){
    return sum( root);
}
int table::sum(node * root){
    if ( !root )
        return 0;
    else{
        return root -> data + sum (root -> left) + sum(root -> right);
    }
}

//STEP 1.5
int table::copy(table & to_copy){
    copy( to_copy.root , root);
}

int table::copy(node * & destination, node * source){
    if ( !source ){
        destination = NULL; 
    }
    else{
        destination = new node;
        destination -> data = source -> data;
        copy(destination -> right, source ->right);
        copy(destination -> left, source -> left);
    }
    return 1;
}


//STEP 2.1 Challenge
int table::create_full(){}
int table::create_full(node * & new_tree){}


//STEP 2.2
bool table::is_full(){}
bool table::is_full(node * root){}


//STEP 2.3
int table::display_largest(){}
int table::display_largest(node * root){}


//STEP 2.4
int table::display_largest2(){}
int table::display_largest2(node * root){}



