#include "table.h"
using namespace std;

int main()
{
    table BST;	
    BST.build();
    BST.display();

    /*  PLACE YOUR FUNCTION CALL HERE */
    BST.count();
    BST.count_leaf();
    BST.remove_largest();
    BST.sum();

    table BSTtocopy;
    BST.copy(BSTtocopy);
    BSTtocopy.display();


    BST.display(0);

    return 0;
}
