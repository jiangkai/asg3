//This class was prepared for a binary search tree implementation of
//a table abstraction. For the lab we will keep the underlying data
//simple - just an integer.
//

#include <iostream>
#include <cstring>
#include <cctype>
#include <cstdlib>

struct node
{
    int data;
    node * left;
    node * right;;
};

class table
{
    public:
	table();
	~table();
	int build(); //All of these functions are provided for you
	int display(int=-99);  //Provided for you. Send in zero for the final result

	//  **** THESE ARE THE WRAPPER Functions that YOU will write!
	int count();               	//STEP 1.1
	int count_leaf();		//STEP 1.2
	int remove_largest();		//STEP 1.3
        int sum();
	int copy(table & to_copy);      //STEP 1.5

	//Challenge
  	int create_full();		//Step 2.1
	bool is_full();			//STEP 2.2
	int display_largest();		//STEP 2.3
	int display_largest2();		//STEP 2.4


    private:

	node * root;

	//  ***These are the functions you will be writing recursively!
	int count(node * root);
	int count_leaf(node * root);
	int remove_largest(node * & root);
	int sum(node * root);
	int copy(node * & destination, node * source);

	//Challenge
        int create_full(node * & new_tree); 
	bool is_full(node * root);
	int display_largest(node * root);
	int display_largest2(node * root);


};






 
  

