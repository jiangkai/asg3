/*
 * author: Kai Jiang
 * Date: Feb 27 2014
 * CS202
 * Assignment 3
 * purpose: building class word class sentence class paragraph
 */

#include <iostream>
#include <cstring>
#include "editor.h"
using namespace std;

//node in paragraph
struct nodeSentence{
	sentence data;
	nodeSentence* next;		//pointer to next nodeSentence
};


//define the class of paragraph
class paragraph
{
public:
	paragraph();
	~paragraph();

	paragraph& operator = (const paragraph& );

	friend ostream& operator << (ostream& , const paragraph& );

	bool operator == (const paragraph& ) const;
	bool operator != (const paragraph& ) const;

	paragraph& operator += (const sentence& );
	paragraph& operator = (const sentence& );

private:
	int cntSentence;
	nodeSentence* head,* tail,* ptr;
};