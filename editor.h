/*
 * author: Kai Jiang
 * Date: Feb 27 2014
 * CS202
 * Assignment 3
 * purpose: building class word class sentence class paragraph
 */

#include <iostream>
#include <cstring>
using namespace std;

class word
{
public:
	word();
	word(char*);
	word(const word	&);
	~word();
	bool operator == (const word&) const;
	bool operator != (const word&) const;

	word& operator = (const word&);
	word operator + (const word&) const;
	word operator + (char*);
	word& operator += (const word&);
	word& operator += (char*);

	friend word operator + (const word&, const word&);
	friend word operator + (const word&, char*);
	friend word operator + (char* ,const word&);

	friend istream& operator >> (istream&, word&);
	friend ostream& operator << (ostream&, const word&);
	
	int length() const;				//return the number of characters in word
	void capital();					//capitalize the word
	bool endingCommas() const;		//if the word is ending with comma or semicolon return true
protected:
	char *w;						//char pointer to store word
	int len;						//the number of characters in word
};

struct node
{
	word wd;
	node* next;

};


//define the class linear linked list
class LLL{
public:
	LLL();
	~LLL();
protected:
	node* tail,* ptr;
};

//define the class sentence
class sentence: public LLL
{
public:
	sentence();
	sentence(const sentence&);
	sentence(const word&);
	~sentence();

	sentence& operator = (const sentence&);
	sentence& operator = (const word&);

	friend ostream& operator << (ostream&, const sentence&);
	friend istream& operator >> (istream&, sentence&);

	bool operator == (const sentence&) const;
	bool operator != (const sentence&) const;

	friend sentence operator + (const sentence&, const sentence&);
	friend sentence operator + (const sentence&, const word&);
	friend sentence operator + (const word&, const sentence&);

	sentence& operator += (const sentence&);
	sentence& operator += (const word&);

	word& operator [] (int) const;

	void capitalization();

	void warningLong();

	int length() const;

protected:
	node* head;
	int cntWord;
	int cntComma;
};